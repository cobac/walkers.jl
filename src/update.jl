function update!(universe::Universe; params::Params)
    if length(universe.walkers) == 0
        println("All walkers are dead")
        return universe
    end 
    # Iterate backwards to avoid index problems with deletions/additions
    for i in length(universe.walkers):-1:1
        update!(universe, i, params = params)
    end 
    return universe
end 

function update!(u::Universe, i::Int; params::Params)
    @unpack space_size,
    step_size,
    p_turn, angle_turn,
    deposit_rate,
    p_division, angle_division,
    threshold_death,
    p_death = params

    if rand() < p_death
        deleteat!(u.walkers, i)
        return u
    end 

    w = u.walkers[i]
    x₀ = w.x
    y₀ = w.y
    
    if rand() < p_turn
        w.θ = (w.θ + rand([-1,1])*( angle_turn + rand(Normal()))) % 2π |> abs
    end 

    w.x = x₀ + cos(w.θ) * step_size |> round |> x -> max(1, x)
    w.y = y₀ + sin(w.θ) * step_size |> round |> x -> max(1, x)

    points = getpoints(x₀, y₀, w.x, w.y, space_size)

    for p in points[2:end]
        u.space[p.x, p.y] = min(1, u.space[p.x, p.y] + deposit_rate)
        if u.space[p.x, p.y] > threshold_death
            deleteat!(u.walkers, i)
            return u
        end 
    end 

    w.x = max(1, (w.x % space_size))
    w.y = max(1, (w.y % space_size))
    
    if rand() < p_division
        w.θ = (w.θ + angle_division) % 2π
        push!(u.walkers,
              Walker(w.x, w.y, (w.θ - angle_division) % 2π))
    end 
    return u
end 
