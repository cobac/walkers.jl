function getpoints(x₀, y₀, x, y, size)
    points = DiscreteP[]
    # Does not interpolate the exact position, just modulus
    # Shouldn't look weird with big sizes
    # Case with x > size && y > size unlikely
    # if (x₀ - x) == 0 && (y₀ - y) == 0
        # return points
    # end 
    if x > size || x <= 0
        x = max(1, mod(x, size))
        Bresenham.line(x₀, y₀, size, y) do a, b
            push!(points,DiscreteP(a,b))
        end 
        Bresenham.line(1, y₀, x, y) do a, b
            push!(points,DiscreteP(a, b))
        end 
    elseif y > size || y <= 0
        y = max(1, mod(y, size))
        Bresenham.line(x₀, y₀, x, size) do a, b
            push!(points,DiscreteP(a, b))
        end 
        Bresenham.line(x₀, 1, x, y) do a, b
            push!(points,DiscreteP(a, b))
        end 
    else
        Bresenham.line(x₀, y₀, x, y) do a, b
            push!(points,DiscreteP(a, b))
        end 
    end 
    return points
end 
