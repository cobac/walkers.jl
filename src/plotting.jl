function plotuniverse(universe::Universe)
    h = heatmap(universe.space, colormap = :greys, 
                axis = (aspect = AxisAspect(1),))
    hidedecorations!(h.axis)
    hidespines!(h.axis)
    display(h)
end 
