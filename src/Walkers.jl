module Walkers

using GLMakie
using Random, Distributions, Colors, Parameters
import Bresenham

export
    Walker,
    Universe,
    Params,
    update!,
    plotuniverse,
    update!,
    # nope
    getpoints,
    DiscreteP

@with_kw struct Params
    space_size::Int64 = 1000
    initial_walkers::Int64 = 10
    step_size::Int64 = 1
    p_turn::Float64 = 0.04
    angle_turn::Float64 = 2π/8
    deposit_rate::Float64 = 0.1
    p_division::Float64 = 0.03
    angle_division::Float64 = 2π/8
    threshold_death::Float64 = 0.8
    p_death::Float64 = p_division * 0.8
end 

mutable struct Walker
    x::Int64
    y::Int64
    θ::Float64
end 
Walker(p::Params) = Walker(rand(1:p.space_size), rand(1:p.space_size), rand(Uniform(0, 2π)))
Walker(θ::Float64, p::Params) = Walker(rand(1:p.space_size), rand(1:p.space_size), θ)


struct Universe
    walkers::Vector{Walker}
    space::Matrix{Float64}
end 

function Universe(n::Int, p::Params)
    @unpack space_size, deposit_rate = p
    walkers = [Walker(p) for w in 1:n]
    s = zeros(space_size, space_size)
    for w in walkers
        s[w.x, w.y] = deposit_rate
    end 
    return Universe(walkers, s)
end 

struct DiscreteP
    x::Int64
    y::Int64
end 

include("plotting.jl")
include("update.jl")
include("path.jl")
end #module
